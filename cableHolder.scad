/*******************************************************************************
 # A generic cable holder
 *
 * Usage: Change the settings below according to the requirements, render the
 *      model, export it, print it and enjoy ;)
 * Note that when the model renders, the base size of the model will be
 *      printed, this should make it easy to see if the customization will
 *      fit the 3D printer.
 *
 # Definitions that should make is easier to understand names of the parameters:
 *
 *  The *back plate* is the part of the holder that hangs against the wall
 *  The *bottom* is the... the bottom part... biggest thing in the XY plane.
 *  *Sargs*: I have no idea what this is called in English and even google 
 *      translate could not help be, so I just took the Danish word instead.
 *      The *sargs* are support flanged that give the individual holders their
 *      strength.
 *  *Landing* is the area of the bottom plate between the slot and the sarg.
 *  The *front* is the stop block that prevents the cables from falling off the 
 *      cable holder.
 *  The *slots* are the slots where the cable will be hanging in.
 *  The *rosettes* are the reenforced parts of the back plate around the 
 *      screw holes.
 *
 # Some abbreviations:
 *
 *  *h*: height
 *  *w*: width
 *  *t*: thickness
 *  *r*: radius
 *  *d*: diameter
 *  *l*: length
 */

/*   
 * ---- v ---- v ---- Customization settings below ---- v ---- v ---- v ---- 
 */ 
t_back_plate = 3;       //< Thickness of the back plate
h_back_plate = 20;      //< Height of the back plate
t_bottom = 2;           //< Thickness of the bottom plate

w_landing = 5;          //< Width of the landings

h_sarg = 4;             //< How high the sargs will be above the bottom plate
w_sarg = 3;             //< Width of the sargs
// The radius of the fillet between the sargs and the back plate. Note that this
// will also be the distance from the backplate to the start of the slot.
r_sarg = 8; 

h_front = 5;            //< Height of the fronts
r_front = 3;            //< Radius of the top corners of the fronts

// The width of each individual slot. Note the order is opposite to what will
// be rendered
w_slot = [11, 11, 8, 8, 8, 8, 5, 5, 5, 5, 5, 5]; 
// Length of the slots - not the depth of the entire cable holder
l_slot = 55;
// The position of all screw holes. The screw holes will be placed in the
// middle between two sargs. The unit of measure is slot number, where slot 0
// is furthest to the right, slot 1 is the second from the right. Slot -1 is
// the last slot (or first from the left), etc.
hole_positions = [1, -2];

w_rosette = 12;         //< Width of the widest part of the rosette
h_rosette = 2;          //< How far the rosette protrudes from the back plate
// The distance from the top of the rosette to the top of the back plate
rosette_below_top = 1;
d_screw_hole = 4;       //< Diameter of the screw holes
d_screw_head = 8;       //< Diameter of the head of the screws
depth_screw_head = 2;   //< How far the screws will be countersunk

/* [Hidden] */
$fn = 50;               //< Number of fragments in the circles

/*   
 * ---- ^ ---- ^ ---- Customization settings above ---- ^ ---- ^ ---- ^ ---- 
 */ 

/*******************************************************************************
 * Implementation
 */

M1 = 0.01; M2 = 2*M1;   //< Just some constants to make difference work better

module front(w, h, t, r1=0, r2=0) {    
    _r1 = r1 == 0 ? M1 : r1;
    _r2 = r2 == 0 ? M1 : r2;    
    
    hull() {
        cube([w, t, 1]);
        translate([r1, 0, h-r1])
            rotate([-90, 0, 0])
                cylinder(h=t, r=_r1);
        translate([w-r2, 0, h-r2])
            rotate([-90, 0, 0])
                cylinder(h=t, r=_r2);
    }
}

module fillet(w, r) {
    difference() {
        cube([w, r, r]);
        translate([-M1, r, r])
            rotate([0, 90, 0])
                cylinder(h=w+M2, r=r);
    }
}

function holder_width(index) =  
    index < 0 ? holder_width(len(w_slot)+index) :
        2 * (w_sarg + w_landing) + w_slot[index];

function holder_position(index, sum=0) = 
        index < 0 ? holder_position(len(w_slot)+index) :
        index == 0 ? sum
            : holder_position(
                index-1, sum+holder_width(index-1)-w_sarg
            );

module one_holder(i) {
    w_tp = holder_width(i);
    l_tp = t_bottom + l_slot + r_sarg + t_back_plate;

    difference() {
        union() {
            // bottom
            cube([w_tp, l_tp, t_bottom]);
            // sarg
            translate([0, 0, t_bottom]) {
                cube([w_sarg, l_tp, h_sarg]);
                translate([0, t_back_plate, h_sarg])
                    fillet(w_sarg, r_sarg);
            }
            translate([w_tp-w_sarg, 0, t_bottom]) {
                cube([w_sarg, l_tp, h_sarg]);
                translate([0, t_back_plate, h_sarg])
                    fillet(w_sarg, r_sarg);
            }
            // front
            translate([0, l_tp-t_bottom, 0]) {
                translate([w_tp-(w_landing+w_sarg), 0, 0])
                    front(w_landing+w_sarg, h_front+t_bottom, t_bottom, r1=r_front);
                front(w_landing+w_sarg, h_front+t_bottom, t_bottom, r2=r_front);
            }
            // back
            cube([w_tp, t_back_plate, h_back_plate]);
        }
        
        // slot
        hull() {
            translate([w_tp/2, t_back_plate+w_slot[i]/2+r_sarg, -M1])
                cylinder(h=t_bottom+M2, d=w_slot[i]);
            translate([w_tp/2, l_tp, -M1])
                cylinder(h=t_bottom+M2, d=w_slot[i]);
        }
    }
}

module screw_rosette(s, h, d_hole, d_head, depth_head) {
    _s = sqrt(2)*s;
    
    /*translate([s/2, s/2, 0])*/ {
        difference() {
            // Rosette
            rotate([0, 0, 45])
                cylinder(h=h, d1=_s, d2=_s-2*h,  $fn=4);
            // Screw head hole
            translate([0, 0, h-depth_head-M1])
                cylinder(h=h+M2, d=d_head);            
        }
    }
}

function rosette_position(i) = [
            holder_position(i)+holder_width(i)/2,
            t_back_plate,
            h_back_plate-w_rosette/2-rosette_below_top,
        ];


/**
 * Construct the holder
 */
difference() {
    union() {
        // the holder
        for(i=[0: len(w_slot)-1]) {
            translate([holder_position(i), 0, 0])
                one_holder(i);
        }
        // Rosettes
        for(i=hole_positions) {
            translate(rosette_position(i)) {
                rotate([-90, 0, 0])
                    screw_rosette(w_rosette, h_rosette, d_screw_hole, d_screw_head, depth_screw_head);
            }
        }
    }
    // Screw holes
    for(i=hole_positions) {
        translate(rosette_position(i)-[0, t_back_plate+M2, 0]) {
            rotate([-90, 0, 0])
                cylinder(h=t_back_plate+h_rosette+M2, d=d_screw_hole);
        }
    }
}

/**
 * Write some information about the holder
 */
x = holder_position(len(w_slot)-1)+holder_width(len(w_slot)-1);
y = t_back_plate + r_sarg + l_slot + t_bottom;
echo("Total base size: ", x, "x", y, " mm");
