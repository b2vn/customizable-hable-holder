# Customizable cable holder
A generic cable holder in OpenSCAD. Change the settings below according to the requirements, render the model, export it, print it and enjoy ;)

Project links:
 * [gitlab](https://gitlab.com/b2vn/customizable-hable-holder)
 * [Thingiverse](https://www.thingiverse.com/thing:4089728/)

Note that when the model renders, the base size of the model will be printed, this should make it easy to see if the customization will fit the 3D printer.

## Readers guide
Knowing these should make is easier to understand names of the parameters.

### Definitions
 * The *back plate* is the part of the holder that hangs against the wall
 * The *bottom* is the... the bottom part... biggest thing in the XY plane.
 * *Sargs*: I have no idea what this is called in English and even google translate could not help be, so I just took the Danish word instead.
 * The *sargs* are support flanged that give the individual holders their strength.
 * *Landing* is the area of the bottom plate between the slot and the sarg.
 * The *front* is the stop block that prevents the cables from falling off the cable holder.
 * The *slots* are the slots where the cable will be hanging in.
 * The *rosettes* are the reenforced parts of the back plate around the screw holes.

### Abbreviations
 * *h*: height
 * *w*: width
 * *t*: thickness
 * *r*: radius
 * *d*: diameter
 * *l*: length

## Parameters
 * Plates
	- `t_back_plate`: Thickness of the back plate
	- `h_back_plate`: Height of the back plate
	- `t_bottom`: Thickness of the bottom plate
	- `w_landing`: Width of the landings
 * Sargs
	- `h_sarg`: How high the sargs will be above the bottom plate
	- `w_sarg`: Width of the sargs
	- `r_sarg`: The radius of the fillet between the sargs and the back plate. Note that this will also be the distance from the backplate to the start of the slot.
 * Front:
	- `h_front`: Height of the fronts
	- `r_front`: Radius of the top corners of the fronts
 * SLots:
	- `w_slot`: (array) The width of each individual slot. Note the order is opposite to what will be rendered
	- `l_slot`: Length of the slots - not the depth of the entire cable holder
 * Srew holes:
 	- `hole_positions`: (array) The position of all screw holes. The screw holes will be placed in the middle between two sargs. The unit of measure is slot number, where slot 0 is furthest to the right, slot 1 is the second from the right. Slot -1 is the last slot (or first from the left), etc.
	- `d_screw_hole`: Diameter of the screw holes
	- `d_screw_head`: Diameter of the head of the screws
	- `depth_screw_head`: How far the screws will be countersunk
 * Rosette:
	- `w_rosette`: Width of the widest part of the rosette
	- `h_rosette`: How far the rosette protrudes from the back plate
	- `rosette_below_top`: The distance from the top of the rosette to the top of the back plate
